package hu.davidhalma.scrumpoker.model

data class Vote(val channelId: String, val authorName: String, val storyPoint: Int)
