package hu.davidhalma.scrumpoker.service

import hu.davidhalma.scrumpoker.exception.AlreadySelectedException
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import org.apache.commons.cli.*
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.io.PrintWriter
import java.io.StringWriter

@Service
class CommandLineService {

    @Value("\${app.prefix}")
    private lateinit var prefix: String

    enum class Command(val opt: String, val longOpt: String) {
        VOTERS("v", "voters"),
        TITLE("t", "title"),
        START("s", "start"),
        END("e", "end"),
        RESTART("r", "restart"),
        HELP("h", "help");
    }

    fun commandLine(event: MessageReceivedEvent): CommandLine {
        try {
            return DefaultParser().parse(getOptions(), event.message.contentRaw.removePrefix("$prefix ").split(" ").toTypedArray())
        } catch (e : org.apache.commons.cli.AlreadySelectedException) {
            throw AlreadySelectedException(event.channel, e.message)
        }
    }

    fun getOptions(): Options {
        val optionGroup = OptionGroup()
            .addOption(Option(Command.START.opt, Command.START.longOpt, false, "Starts a new scrum poker."))
            .addOption(Option(Command.END.opt, Command.END.longOpt, false, "Ends the currently running scrum poker on channel and prints the results."))
            .addOption(Option(Command.RESTART.opt, Command.RESTART.longOpt, false, "Restart the scrum poker and prints the results."))
            .addOption(Option(Command.HELP.opt, Command.HELP.longOpt, false, "Prints this help message."))
        val voterOptionGroup = OptionGroup()
            .addOption(Option(Command.VOTERS.opt, Command.VOTERS.longOpt, true, "Maximum number of voters."))
        val titleOption = Option.builder(Command.TITLE.opt)
            .longOpt(Command.TITLE.longOpt)
            .hasArgs()
            .optionalArg(true)
            .type(String::class.java)
            .desc("Title of the story.")
            .build()
        val titleOptionGroup = OptionGroup()
            .addOption(titleOption)
        return Options()
            .addOptionGroup(optionGroup)
            .addOptionGroup(voterOptionGroup)
            .addOptionGroup(titleOptionGroup)
    }

    fun getHelp(): MutableList<String> {
        val out = StringWriter()
        val writer = PrintWriter(out)
        HelpFormatter().printHelp(writer, 9999, "null", null, getOptions(), 0, 0, null)
        writer.flush()
        val lines = out.toString().split(System.lineSeparator()).toMutableList()
        writer.close()
        out.close()
        lines.removeIf { !it.startsWith("-") }
        println(lines)
        return lines
    }

    fun getUsage(): String {
        val out = StringWriter()
        val writer = PrintWriter(out)
        HelpFormatter().printUsage(writer,9999, prefix, getOptions())
        writer.flush()
        val toString = out.toString()
        writer.close()
        out.close()
        return toString.removePrefix("usage: ")
    }
}