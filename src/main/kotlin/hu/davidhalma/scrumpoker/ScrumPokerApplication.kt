package hu.davidhalma.scrumpoker

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ScrumPokerApplication

fun main(args: Array<String>) {
    runApplication<ScrumPokerApplication>(*args)
}
