# Discord Scrum Poker

[Docker image](https://hub.docker.com/r/amaraa44/discord-scrumpoker)

### Usage (args are optional):
```
!sp [-e | -h | -r | -s] [-t <arg>] [-v <arg>]
-e,--end
Ends the currently running scrum poker on channel and prints the results.
-h,--help
Prints this help message.
-r,--restart
Restart the scrum poker and prints the results.
-s,--start
Starts a new scrum poker.
-t,--title
Title of the story.
-v,--voters
Maximum number of voters.
```

### Example usage:
```
!sp -s -v 5 -t Example story title
!sp -r
!sp -e
!sp -h
```

### Example docker-compose file:
```
version: "3.9"
services:
  app:
    image: "amaraa44/discord-scrumpoker:0.0.2"
    environment:
      BOT_TOKEN: "xxxxxxxx.xxxxxx" # required
      APP_PREFIX: "!sp" # optional (Default: "!sp")
      APP_MENTION_CHANNEL: "true" # optional (Default: "true")
      APP_MENTION_USER: "true" # optional (Default: "true")
```

### Setup
- You have to create a `BOT_TOKEN` on [Discord Developer Portal](https://discord.com/developers). Create a new application and then you can get the token under the `bot` menu.
- Generate an OAuth2 link in `OAuth2` menu. Check the `bot` option and check the `Manage Messages` option. Open the generated link and add to your server.
